import QtQuick 2.12
import Lomiri.Components 1.3 as UITK
import Lomiri.DownloadManager 1.2 as UDM

Rectangle {
    id: downloadManager
    anchors {
        left: parent.left
        right: parent.right
    }
    color: "#fff"

    signal download(string downloadUrl, var headers)
    signal fileDownloaded(string downloadPath)

    Component.onCompleted: {
        download.connect((downloadUrl, headers) => {
            console.log("Downloading file:", downloadUrl);
            /*
            // multipleDownload.headers = headers;
            multipleDownload.download(downloadUrl);
            */

            console.log("headers", JSON.stringify(headers));
            singleDownload.headers = headers;
            singleDownload.download(downloadUrl);
        });
    }

    UITK.ProgressBar {
        width: parent.width
        height: units.gu(2)

        minimumValue: 0
        maximumValue: 100

        value: singleDownload.progress
        visible: singleDownload.downloading
    }

    Component {
        id: downloadItemDelegate
        UITK.ProgressBar {
            width: parent.width
            height: units.gu(2)

            minimumValue: 0
            maximumValue: 100

            value: modelData.progress
            visible: modelData.downloadInProgress
        }
    }

    ListView {
        id: list
        anchors {
            fill: parent
        }
        model: multipleDownload.downloads
        delegate: downloadItemDelegate
    }

    UDM.DownloadManager {
        id: multipleDownload
    }

    Connections {
        target: multipleDownload
        onDownloadFinished: (downloadType, filePath) => {
            fileDownloaded(filePath);
        }
    }

    UDM.SingleDownload {
        id: singleDownload
    }

    Connections {
        target: singleDownload
        onFinished: (filePath) => {
            fileDownloaded(filePath);
        }
    }
}
